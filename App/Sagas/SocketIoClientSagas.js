/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put, take, cancelled, select, race } from 'redux-saga/effects'
import { eventChannel } from 'redux-saga'
import io from 'socket.io-client'
import SocketIoClientActions, { SocketIoClientTypes }  from '../Redux/SocketIoClientRedux'
import { Toast } from 'native-base'; 
// import StaticVar from '../Config/StaticVar';
// import AsyncStorageHelper from '../Lib/AsyncStorageHelper';
// import SyncStorage from 'sync-storage'
// import AuthActions from '../Redux/AuthRedux'
// import MyWorkAction from '../Redux/MyWorkRedux';
// import MyWorkSpvAction from '../Redux/MyWorkSpvRedux';
// import GetSafetyNewsActions from '../Redux/GetSafetyNewsRedux'
// import PlayFinishSound from '../Lib/PlayFinishSound';
// import { SocketIoClientSelectors } from '../Redux/SocketIoClientRedux'

// export function * getSocketIoClient (api, action) {
//   const { data } = action
//   // get current data from Store
//   // const currentData = yield select(SocketIoClientSelectors.getData)
//   // make the call to the api
//   const response = yield call(api.getsocketIoClient, data)

//   // success?
//   if (response.ok) {
//     // You might need to change the response here - do this with a 'transform',
//     // located in ../Transforms/. Otherwise, just pass the data back from the api.
//     yield put(SocketIoClientActions.socketIoClientSuccess(response.data))
//   } else {
//     yield put(SocketIoClientActions.socketIoClientFailure())
//   }
// }

let socket
const connect = (socketUrl, userobject) => {  
  // let socket = io.connect(socketUrl, 
  socket = io.connect(socketUrl, 
    {
      // 'forceNew': true,
      query: 'userobject='+JSON.stringify(userobject),
      // reconnection: true,
      // reconnectionDelay: 1000,
      // reconnectionAttempts: Infinity,
      transports: ['websocket']
    }
  )
  return new Promise((resolve) => {
    socket.on('connect', () => {
      console.log('Saga - socket connect')      
      resolve(socket)
    })
  })
}
 
function createSocketChannel() {
  return eventChannel(emit => {
      
    //callback  
    const addwo = (note) => {
      // const user = SyncStorage.get(StaticVar.DB_KEY_USER).user 
      console.log('add wo', note)
      // console.log('user id socket', user)
      // // Toast.show({
      // //   text: (note.action),
      // //   duration: 2500,
      // //   type: 'success',
      // //   position: 'top'
      // // })
      // //Perubahan note
      // if (note.action) {
      //   //delete wo
      //   if (note.action.toLowerCase() === 'delete wo'){
      //     if (note.data.emitter !== user._id) { 
      //       // broadcast di terima oleh bukan pemancar
      //       // Lakukan delete mywork disini CHECK
      //       emit(MyWorkAction.localDeleteMyWorkRequest(note.data._id))
      //     } else {
      //       // broadcast di terima oleh pemancar  
      //     }           
      //     // new wo terbuat
      //   } 
      //   else if (note.action.toLowerCase() === 'cc done') {
      //     console.log('INI', note.data.emitter, user._id)
      //     if (note.data.emitter !== user._id) { 
      //       // broadcast bukan di terima oleh pemancar
      //       // Jalankan aksi getMyWorkRequest & bunyikan lagu 
      //       PlayFinishSound()
      //       // emit(MyWorkAction.getMyWorkRequest(user._id))
      //     }  
      //   } 
      //   else {
      //     emit(MyWorkAction.getMyWorkRequest(user._id))
      //   }
      // } else {
      //   emit(MyWorkAction.getMyWorkRequest(user._id))
      // }
      // emit(MyWorkSpvAction.getMyWorkSpvRequest(user._id))
      // AsyncStorageHelper(StaticVar.DB_KEY_USER).getDataObject().then(session => {
      //   const user = session
      //   console.log('add wo', data)
      //   console.log('user id socket', user)
      //   Toast.show({
      //     text: (data.action),
      //     duration: 2500,
      //     type: 'success'
      //   })
      //   emit(MyWorkAction.getMyWorkRequest(user._id)) //kita emit action ini, dia nanti akan dikirim ke baris 128
      // })    
    }

    const answercc = async(data) => {   //kalau bisa, tidak usah tambahkan async (lihat function baris 67 diatas)
      // const user = SyncStorage.get(StaticVar.DB_KEY_USER).user 
      console.log('Answer CC', JSON.stringify(data))
      // console.log('user id socket', user)
      // if(data.user_id !== user._id){
      //   console.log('bukan pengirim', data.user_id, user._id, JSON.stringify(data.spvwork))
      //   if (user._id === data.spvwork && data.answer === 'NO') {
      //     PlayFinishSound()
      //   }
      //   // Toast.show({
      //   //   text: 'Pengecekan '+data._id,
      //   //   duration: 2500,
      //   //   type: 'success',
      //   //   position: 'top'
      //   // })
      //   // emit(MyWorkAction.getCheckMyWorkRequest(data._id))
      //   emit(MyWorkAction.getMyWorkRequest(user._id))
      // }
      // else{
      //   console.log('pengirim', data.user_id, user._id)
      // }
      
      
      // emit(MyWorkSpvAction.getMyWorkSpvRequest(user._id))
      // const user = SyncStorage.get(StaticVar.DB_KEY_USER).user
      // console.log('Answer CC', data)
      // console.log('user id socket', user)
      // MyWorkAction.getMyWorkRequest(user._id);\
    }

    const fromserver = (data) => {
      console.log('fromserver', (data))
      Toast.show({
        text: (data),
        duration: 2500,
        type: 'success'
      })
    }
    
    const news = (data) => {
      console.log(data)
      Toast.show({
        text: 'Pembaruan Info Safety',
        duration: 2000,
        type: 'success'
      })
      // emit(GetSafetyNewsActions.getSafetyNewsRequest())
    }

    const forcelogout = (data) => {
      //console.log('testing update news', data)
      Toast.show({
        text: data,
        duration: 2000,
        type: 'success'
      })
      // emit(AuthActions.authLogoutRequest())
    } 

    //listener
    socket.on('fromserver', fromserver) 
    socket.on('add wo', addwo) 
    socket.on('Answer CC', answercc) 
    socket.on('news', news) 
    socket.on('forcelogout', forcelogout)
    return () => {
      //unsubmit listener disini 
      socket.off('fromserver', fromserver) 
      socket.off('add wo', addwo) 
      socket.off('Answer CC', answercc) 
      socket.off('news', news) 
      socket.off('forcelogout', forcelogout)
      // socket.off('onLeaveThenDisconect', onLeaveThenDisconect)
    }
  }//, buffers.sliding(2)
  )
}
 
export function * socketIoClientChannel (socketUrl, userobject) { 
  yield call(connect, socketUrl, userobject) //versi glbal
  // connect(socketUrl)
  let socketChannel = yield call(createSocketChannel)//, socket)
  try {
    while (true) {
      const action = yield take(socketChannel)  // terima emit-an dari socket callback
      yield put(action)                         // eksekusi action disini
    }
  } catch (error) {
    console.log(error)
  } finally {
    if (yield cancelled()) { 
      socketChannel.close()
      yield socket.disconnect()
      socket.destroy()
      // socket = null 
      console.log('socket terdiss')      
      // yield put({type: CHANNEL_OFF})
    }
  }
}

// async function getSession() {
  // const session = await AsyncStorageHelper(StaticVar.DB_KEY_USER).getDataObject() 
  // return session
// }
 
//Start
export function * socketIoClientConnect (action) {
  const { socketurl, userobject } = action //versi lite ini userobject di passing dari caller, dan tanpa pengecekan kode dibawah
  // console.log('socketIoClientConnect', socketurl)
  
  // while (true) {
  // console.log('phgase 1') 

  // let userobject = null //versi lite ini di isi langunsg, dan tampa pengecekan kode dibawah
  // let session = yield call (getSession)
  // if (session) {
  //   // console.log('adakah user ?', session.user)
  //   userobject = session.user
  // }

  // console.log('Lewat Check user')   

  yield race({
    task: call(socketIoClientChannel, socketurl, userobject),
    cancel: take(SocketIoClientTypes.SOCKET_IO_CLIENT_DISCONNECT),
  })
}

//EMIT

export function * socketIoClientJoin (action) {
  const { userobject } = action
  // console.log('ini join', userobject)  
  
  yield socket.emit('join', userobject)
}
export function * socketIoClientLeave (action) {
  const { userobject } = action
  // console.log('ini leave', userobject)  
  yield socket.emit('leave', userobject)
}

export function * socketIoClientAddWoNote (action) {
  const { target, note } = action
  console.log('emit - add wo', target, note)  
  yield socket.emit('add wo', target, note)
}

export function * socketIoClientAnswerCC (action) {
  const { target, note } = action 
  yield socket.emit('Answer CC', target, note)
}

// export function * socketIoClientLeave (action) {
//   const { userobject } = action
//   console.log('ini leave', userobject)
  
//   yield socket.emit('leave', userobject)
// }

// export function * socketIoRoomList () {  
//   yield socket.emit('roomlist')
// }

// export function * socketIoSendMessage (action) {  
//   const { userid, msg } = action
//   console.log('send msg', userid, msg)  
//   yield socket.emit('sendmessage', { userid, msg })
// }

// export function * socketIoPingToAll () {   
//   console.log('ping all')    
//   yield socket.emit('pingall')
// }
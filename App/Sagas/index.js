import { takeLatest, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { GithubTypes } from '../Redux/GithubRedux'
import { AuthTypes } from '../Redux/AuthRedux'
import { HandleResponseErrorTypes } from '../Redux/HandleResponseErrorRedux' 
import { SocketIoClientTypes } from '../Redux/SocketIoClientRedux'


/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { getUserAvatar } from './GithubSagas'
import { 
  authSessionCheck,
  authRequest,
  authLogoutRequest,
  authSessionEnded,
  //test only
  // test
} from './AuthSagas'
import { handleResponseError } from './HandleResponseErrorSagas'
import { 
  socketIoClientConnect,
  socketIoClientJoin,
  socketIoClientLeave,
  socketIoClientAddWoNote,
  socketIoClientAnswerCC,
} from './SocketIoClientSagas'
  


/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    // some sagas receive extra parameters in addition to an action
    takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api),

    takeLatest(HandleResponseErrorTypes.HANDLE_RESPONSE_ERROR, handleResponseError), 

    //AUTH
    takeLatest(AuthTypes.AUTH_SESSION_CHECK, authSessionCheck, api),
    takeLatest(AuthTypes.AUTH_REQUEST, authRequest, api),
    takeLatest(AuthTypes.AUTH_LOGOUT_REQUEST, authLogoutRequest, api),
    takeLatest(AuthTypes.AUTH_SESSION_ENDED, authSessionEnded),

    //Socket io
    takeLatest(SocketIoClientTypes.SOCKET_IO_CLIENT_CONNECT, socketIoClientConnect),
    takeLatest(SocketIoClientTypes.SOCKET_IO_CLIENT_JOIN, socketIoClientJoin),
    takeLatest(SocketIoClientTypes.SOCKET_IO_CLIENT_LEAVE, socketIoClientLeave),    
    takeLatest(SocketIoClientTypes.SOCKET_IO_CLIENT_ADD_WO_NOTE, socketIoClientAddWoNote),    
    takeLatest(SocketIoClientTypes.SOCKET_IO_CLIENT_ANSWER_CC, socketIoClientAnswerCC),
  ])
}

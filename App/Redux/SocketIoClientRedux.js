import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  // socketIoClientRequest: ['data'],
  // socketIoClientSuccess: ['payload'],
  // socketIoClientFailure: null,

  socketIoClientConnect: ['socketurl', 'userobject'],
  socketIoClientJoin: ['userobject'],
  socketIoClientLeave: ['userobject'],
  socketIoClientAddWoNote: ['target', 'note'],
  socketIoClientAnswerCC: ['target', 'note'],

  socketIoClientDisconnect: null,
  // socketIoRoomList: null,
  // socketIoPingToAll: null,
  // socketIoSendMessage: ['userid', 'msg'],
  // socketIoClientSetRoomParticipant: ['participant'],
  // socketIoClientForceToRoleSelect: null,

  socketIoResetState: null,
})

export const SocketIoClientTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  participant: {}
})

/* ------------- Selectors ------------- */

export const SocketIoClientSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
// export const request = (state, { data }) =>
//   state.merge({ fetching: true, data, payload: null })

//   export const setparticipant = (state, { participant }) =>
//   state.merge({ participant })  

// export const connect = (state, { socketurl, userobject, ishost }) =>
//   state.merge({ fetching: true, data: {socketurl, userobject, ishost}, payload: null }) 
// successful api lookup
// export const success = (state, action) => {
//   const { payload } = action
//   return state.merge({ fetching: false, error: null, payload })
// }

// Something went wrong somewhere.
// export const failure = state =>
//   state.merge({ fetching: false, error: true, payload: null })

export const reset = () => INITIAL_STATE
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  // [Types.SOCKET_IO_CLIENT_REQUEST]: request,
  // [Types.SOCKET_IO_CLIENT_SUCCESS]: success,
  // [Types.SOCKET_IO_CLIENT_FAILURE]: failure,
  // [Types.SOCKET_IO_CLIENT_CONNECT]: connect,
  // [Types.SOCKET_IO_CLIENT_JOIN]: join,
  // [Types.SOCKET_IO_CLIENT_SET_ROOM_PARTICIPANT]: setparticipant,

  // [Types.SOCKET_IO_RESET_STATE]: reset,

  // [Types.SOCKET_IO_CLIENT_DISCONNECT]: reset,
})

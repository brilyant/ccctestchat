import React, { Component } from 'react'
import { TouchableOpacity, Text, Image, View, ActivityIndicator } from 'react-native'
import { Icon, Container, Button, Header } from 'native-base';
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import SampleLoginActions from '../Redux/SampleLoginRedux' 
import AuthActions from '../Redux/AuthRedux' 

// Styles
import styles from './Styles/LaunchScreenStyles' 
import { Colors, Metrics } from '../Themes';
import images from '../Themes/Images'
// import ErrorNetworkButton from '../Components/ErrorNetworkButton' 


class LaunchScreen extends Component {
  componentDidMount() { 
    this.doSessionCheck()
  }

  doSessionCheck() {
    // this.props.notificationServiceStart(() => {this.props.authSessionCheck()})
    this.props.authSessionCheck()
  }

  render () {    
    const { fetching, error } = this.props.auth  
    return (
      <Container style={styles.container}>
        <Header androidStatusBarColor={Colors.primaryColor} style={styles.header} />
        <View style={styles.rowLogo}>
          <Text>Tunggu Sebentar</Text>
        </View>
        <View style={styles.rowSubTitle}>
          {
            (fetching === true) ?
              <ActivityIndicator style={Metrics.screenWidth/ 30} color={Colors.primaryColor} size="large" />
              : (error === true) ? 
                <TouchableOpacity onPress={() => this.doSessionCheck() /*this.props.authSessionCheck()*/}>
                  <Text style={styles.subTitle}>Gagal Terhubung ke Server <Text style={styles.subTitle}>... Ulangi ?</Text></Text>             
                </TouchableOpacity>
                : <TouchableOpacity onPress={() => this.doSessionCheck() /*this.props.authSessionCheck()*/}>
                  <Text style={styles.subTitle}>Gagal Terhubung ke Server <Text style={styles.subTitle}>... Ulangi ?</Text></Text>             
                </TouchableOpacity>
          }
        </View>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    // samplelogin: state.samplelogin
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // sampleLoginRequest: (data) => dispatch(SampleLoginActions.sampleLoginRequest(data)),
    authSessionCheck: () => dispatch(AuthActions.authSessionCheck()), 
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)

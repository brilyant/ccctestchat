
const BASE = 'http://192.168.10.11:5444' //DEV
export default {
  Url: BASE,
  ImageUrl: `${BASE}/Fatalrisk/image/`,
  ImagesCheckUrl: `${BASE}/repo/checkimages/`, 
  ImagesRepoUrl: `${BASE}/repo/observer/`, 
  // Url: BASE,
  DB_KEY_SESSION: 'DB@CCCTestChat',
  DB_KEY_USER: 'User',
  DB_KEY_SUPERVISOR: 'Supervisor',
  DB_KEY_WORKORDER: 'StackWorkOrder',
  FCM_TOKEN: '@CCC:KeyFCMToken',
  PING_TARGET : BASE+'/heartbeat',
  PENDING_KEY: {
    ADD_MYWORK: 'addmywork'
  },
  FCM_SENDER_ID: '590953647316', //'590953647316',
  COMPILE_DATE: '02 Juni 2020',
  REV: '7',
}
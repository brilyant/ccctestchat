import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'
import { GiftedChat, Bubble, Time } from 'react-native-gifted-chat'
import ObjectID from 'bson-objectid'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import SocketIoClientActions from '../Redux/SocketIoClientRedux'
// Styles
import styles from './Styles/ChatScreenStyle'
import StaticVar from '../Config/StaticVar'

class ChatScreen extends Component {
  constructor(props) {
    super(props)
    this.state = { 
      messages: []
    }
  }
  componentDidMount() { 
    const username = this.props.navigation.getParam('username')
    const userobject = {
      _id: ObjectID(),            //generate random
      supervisor: ObjectID(),     //generate random
      userid: username,                       //name
      name: username,                         //name
    }
    console.log(userobject)
    this.props.socketIoClientConnect(StaticVar.Url, userobject)
    /*
    const messages = [
      {
        _id: (Math.random()*199),
        text: 'Hello developer',
        createdAt: new Date(),
        user: {
          _id: 2,
          name: 'Bot',
          // avatar: 'https://placeimg.com/140/140/any',
        },
      },
    ]
    this.setState({
      messages: GiftedChat.append(this.state.messages, messages),
    })
    */
  }
  
  componentWillUnmount() {
    this.props.socketIoClientDisconnect()
  }

  onSend(messages = []) {
    this.setState({
      messages: GiftedChat.append(this.state.messages, messages),
    }) 
  }

  renderBubble (props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: {
            backgroundColor: 'yellow', 
          },
          left: {
            backgroundColor: 'white', 
          }
        }}
        textStyle={{
          right: {
            color: 'black', 
          }
        }}
        renderTime={() =>(
          <Time {...props} 
            timeTextStyle={{ 
              right: {
                color: 'gray', 
              } 
            }} />
        )}
      />
    )
  }

  render () {
    const currentuser = this.props.navigation.getParam('username')
    console.log('messages', this.state.messages)
    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={messages => this.onSend(messages)}
        user={{
          _id: currentuser,
          name: currentuser,
          avatar: 'https://placeimg.com/140/140/any',
        }}
        // renderBubble={this.renderBubble}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    socketIoClientConnect: (url, userobject) => dispatch(SocketIoClientActions.socketIoClientConnect(url, userobject)), //buang param tidak perlu
    socketIoClientDisconnect: () => dispatch(SocketIoClientActions.socketIoClientDisconnect()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen)

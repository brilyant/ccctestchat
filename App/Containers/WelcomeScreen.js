import React, { Component } from 'react'
import { ScrollView, Image, View } from 'react-native'
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Input, Form, Item, Label } from 'native-base';
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/WelcomeScreenStyle'
import { Colors, Metrics } from '../Themes';

class WelcomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
    };
    // this.butNext = this.butNext.bind(this)
  }

  render() {
    console.log(this.state.username)
    return (
      <Container>
        <Header noLeft>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Masukan Username</Title>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
          <Text style={{ textAlign: 'center' }}>Masukan Username</Text>
          <Form>
            <Item last={true} stackedLabel>
              <Label>Username</Label>
              <Input 
                value={this.state.username} 
                onChangeText={ username => this.setState({ username })} 
                onSubmitEditing={() =>{
                  this.butNext.props.onPress()
                }}

              />
            </Item> 
          </Form>
        </Content>
        <Footer>
          <FooterTab>
            <Button ref={(button) => { this.butNext = button }}  disabled={!this.state.username} full onPress={() => this.props.navigation.navigate('ChatScreen', {username: this.state.username})}>
              <Text>OK</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeScreen)

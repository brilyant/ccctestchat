import React, { Component } from 'react'
import { View, StatusBar, YellowBox } from 'react-native'
import ReduxNavigation from '../Navigation/ReduxNavigation'
import FlashMessage from 'react-native-flash-message'
import { connect } from 'react-redux'
import StartupActions from '../Redux/StartupRedux'
import ReduxPersist from '../Config/ReduxPersist'
import { Root, StyleProvider } from 'native-base'
import getTheme from '../../native-base-theme/components'
import material from '../../native-base-theme/variables/material'
import SpinnerOverlay from 'react-native-loading-spinner-overlay' 

// Styles
import styles from './Styles/RootContainerStyles'

YellowBox.ignoreWarnings([
  'Warning: componentWillMount is deprecated',
  'Warning: componentWillReceiveProps has been renamed',
])

class RootContainer extends Component {
  componentDidMount () {
    // if redux persist is not active fire startup action
    if (!ReduxPersist.active) {
      this.props.startup()
    }
  }

  render () {
    return (
      <View style={styles.applicationView}>
        <StatusBar barStyle='light-content' />
        <Root>
          <StyleProvider style={getTheme(material)}>
            <ReduxNavigation />
          </StyleProvider>
          <SpinnerOverlay
            visible={this.props.overlayspinner.fetching === true}
            textContent={'Loading...'}
            textStyle={styles.spinnerTextStyle}
            overlayColor={'rgba(192,192,192,0.4)'}
          />
          <FlashMessage position="top" />
        </Root>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    overlayspinner: state.overlayspinner
  }
}

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = (dispatch) => ({
  startup: () => dispatch(StartupActions.startup())
})

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer)

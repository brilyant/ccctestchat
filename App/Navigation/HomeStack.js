import { createStackNavigator } from 'react-navigation-stack' 
// import MainTabNavigator from './MainTabNavigator'
import styles from './Styles/NavigationStyles'
import WelcomeScreen from '../Containers/WelcomeScreen'  
import ChatScreen from '../Containers/ChatScreen'
const HomeStack = createStackNavigator({
  WelcomeScreen:  { screen: WelcomeScreen }, 
  ChatScreen:  { screen: ChatScreen }, 
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'WelcomeScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default HomeStack